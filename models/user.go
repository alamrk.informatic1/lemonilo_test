package models

import (
	"database/sql"
	"net/http"
	"strconv"

	log "github.com/sirupsen/logrus"
	"gitlab.com/alamrk.informatic1/lemonilo_test/db"
)

type User struct {
	UserId   int64  `db:"id" json:"id"`
	Email    string `db:"email" json:"email"`
	Address  string `db:"address" json:"address"`
	Username string `db:"username" json:"username"`
	Password string `db:"password" json:"password"`
	Active   string `db:"active" json:"active"`
}

func GetUserByEmail(c *User, params string) int {
	query := `SELECT
	users.id,
	users.email,
	users.address,
	users.username,
	users.password,
	users.active
	FROM users
	WHERE users.username = ?`

	log.Info(query)
	err := db.Db.Get(c, query, params)
	if err != nil {
		log.Info(err)
		return http.StatusNotFound
	}
	return http.StatusOK
}

func GetUser(u *User, id string) int {
	query := "SELECT id, email, address, username, password, active FROM users WHERE id = " + id
	err := db.Db.Get(u, query)
	log.Println(query)
	if err != nil {
		log.Println(err)
		return http.StatusNotFound
	}
	return http.StatusOK
}

func GetAllUsers(u *[]User, limit uint64, offset uint64, pagination bool, params map[string]string) (uint64, error) {
	query := "SELECT * FROM users"

	var condition string
	// Combine where clause
	clause := false
	for key, value := range params {
		if clause == false {
			condition += " WHERE"
		} else {
			condition += " AND"
		}
		condition += " users." + key + " = '" + value + "'"
		clause = true
	}

	query += condition

	if limit > 0 {
		query += " LIMIT " + strconv.FormatUint(limit, 10)
	}
	if offset > 0 {
		query += " OFFSET " + strconv.FormatUint(offset, 10)
	}

	// Check pagination
	var total uint64
	if pagination == true {
		countQuery := "SELECT COUNT(id) FROM users" + condition
		var totalStr string
		log.Println(countQuery)
		err := db.Db.Get(&totalStr, countQuery)
		if err != nil {
			log.Println(err)
			return 0, err
		}
		total, _ = strconv.ParseUint(totalStr, 10, 64)
	}

	// Main query
	log.Println(query)
	err := db.Db.Select(u, query)
	if err != nil {
		log.Println(err)
		return 0, err
	}
	if pagination == false {
		total = uint64(len(*u))
	}

	return total, nil
}

func CreateUser(params map[string]string) int {
	query := "INSERT INTO users("
	var fields = ""
	var values = ""
	i := 0
	for key, value := range params {
		fields += "`" + key + "`"
		values += "'" + value + "'"
		if (len(params) - 1) > i {
			fields += ", "
			values += ", "
		}
		i++
	}
	query += fields + ") VALUES(" + values + ")"
	tx, err := db.Db.Begin()
	if err != nil {
		log.Println(err)
		return http.StatusBadGateway
	}
	_, err = tx.Exec(query)
	tx.Commit()
	if err != nil {
		log.Println(err)
		return http.StatusBadRequest
	}
	return http.StatusOK
}

func UpdateUser(params map[string]string) int {
	query := "UPDATE users SET "
	// Get params
	i := 0
	for key, value := range params {
		if key != "id" {
			query += key + " = '" + value + "'"
			if (len(params) - 2) > i {
				query += ", "
			}
			i++
		}
	}
	query += " WHERE id = " + params["id"]
	log.Println(query)

	tx, err := db.Db.Begin()
	if err != nil {
		log.Println(err)
		return http.StatusBadGateway
	}
	var ret sql.Result
	ret, err = tx.Exec(query)
	row, _ := ret.RowsAffected()
	if row > 0 {
		tx.Commit()
	} else {
		return http.StatusNotFound
	}
	if err != nil {
		log.Println(err)
		return http.StatusBadRequest
	}
	return http.StatusOK
}

func DeleteUser(id string) int {
	query := "DELETE FROM users WHERE id = " + id
	tx, err := db.Db.Begin()
	if err != nil {
		log.Println(err)
		return http.StatusBadGateway
	}
	var ret sql.Result
	log.Println(query)
	ret, err = tx.Exec(query)
	row, _ := ret.RowsAffected()
	if row > 0 {
		tx.Commit()
	} else {
		return http.StatusNotFound
	}
	if err != nil {
		log.Println(err)
		return http.StatusBadRequest
	}
	return http.StatusOK
}
