package common

import (
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
)

type Configuration struct {
	Host            string `default:":http://localhost" split_words:"true"`
	ListenPort      string `default:":9673" split_words:"true"`
	RootURL         string `default:"/lemonilotest" split_words:"true"`
	MariadbHost     string `default:"localhost" split_words:"true"`
	MariadbPort     string `default:"3306" split_words:"true"`
	MariadbUser     string `default:"root" split_words:"true"`
	MariadbPassword string `default:"" split_words:"true"`
	MariadbDb       string `default:"lemonilo" split_words:"true"`
	Secret          string `default:"lemoniloscreet823" split_words:"true"`
	TokenLifeTime   uint64 `default:"3600" split_words:"true"`
	LimitQuery      uint64 `default:"100" split_words:"true"`
}

var Config Configuration

// LoadConfig .
func LoadConfig() {
	if err := envconfig.Process("LEMONILOTEST", &Config); err != nil {
		log.Fatal(err.Error())
	}
}
