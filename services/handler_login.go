package services

import (
	"encoding/base64"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	cm "gitlab.com/alamrk.informatic1/lemonilo_test/common"
	md "gitlab.com/alamrk.informatic1/lemonilo_test/models"
)

func Login(c echo.Context) error {
	username := c.FormValue("username")
	if username == "" {
		return echo.ErrUnauthorized
	}

	password := c.FormValue("password")
	if password == "" {
		return echo.ErrUnauthorized
	}

	// Throws unauthorized error
	var user md.User
	checkEmail := md.GetUserByEmail(&user, username)
	if checkEmail != http.StatusOK {
		return echo.ErrUnauthorized
	}

	passwordDecode, _ := base64.StdEncoding.DecodeString(user.Password)
	if password != string(passwordDecode) {
		return echo.ErrUnauthorized
	}

	// Set custom claims
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["email"] = user.Email
	claims["password"] = user.Password
	claims["exp"] = time.Now().Add(time.Duration(cm.Config.TokenLifeTime) * time.Second).Unix()

	// Generate encoded token and send it as response.
	t, err := token.SignedString([]byte(cm.Config.Secret))
	if err != nil {
		return err
	}

	res := md.OAuthMessage{
		AccessToken: t,
		TokenType:   "bearer",
		ExpiresIn:   cm.Config.TokenLifeTime,
	}

	return c.JSON(http.StatusOK, res)
}

func accessible(c echo.Context) error {
	return c.String(http.StatusOK, "Accessible")
}
